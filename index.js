//Які існують типи даних у Javascript?
//number - любые числа - целые и с плавающей запятой.
//bigint - целые числа произвольной длины.
//string - для строк. Может содержать любое количество символов (пустая строка "" тоже строка).
//boolean - логический тип. true - правда, false - ложь.
//null - для значений, которые неизвестны.
//symbol - для уникальных идентификаторов.
//undefined - указывает на то, что значение не было присвоено. Если переменная обьявлена но не инициализирована, то вернет undefined.
//object - сложный тип данных (все остальные - примитивные). Позволяет записать в себя сложные структуры данных.

//У чому різниця між == і ===?
//Оператор == (равно) сравнивает два значения и пытается привести их к одному типу данных. Возвращает true или false.
//Оператор === (строгое равно) сравнивает без приведения к единому типу. Возвращает true или false.

//Що таке оператор?
//Внутрення функция JS: + , - , * , / , % , ** , == , === , > , < , ! и так далее.

let userName;
let userAge;

while (!userAge || !userName || userAge <= 0 || !isFinite(userAge)) {
  userName = prompt("Укажите ваше имя", userName);
  userAge = prompt("Укажите ваш возраст", userAge);
}

if (userAge < 18) {
  alert("You are not allowed to visit this website!");
} else if (userAge >= 18 && userAge <= 22) {
  const userChoice = confirm("Are you sure you want to continue?");
  switch (userChoice) {
    case true:
      alert(`Welcome, ${userName}!`);
      break;
    case false:
      alert("You are not allowed to visit this website!");
      break;
    default:
  }
} else if (userAge > 22) {
  alert(`Welcome, ${userName}!`);
}
